﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using BikeShopFront.Helpers;
using BikeShopFront.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;

namespace BikeShopFront.Controllers
{
    [Route("cart")]
    public class CartController : Controller
    {
        [Route("index")]
        public IActionResult Index()
        {
            try
            {
                var cart = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "cart");
                ViewBag.cart = cart;
                ViewBag.total = cart.Sum(item => item.Bicycles.Listprice * item.Quantity);
            }
            catch
            {
                ViewBag.total = 0;
                return View();
            }

            return View();
        }

        [Route("buy/{id}")]
        public async System.Threading.Tasks.Task<IActionResult> Buy(decimal id)
        {
            Bicycles bicycleList = new Bicycles();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("http://18.218.138.221/api/bicycles/"  + id))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    bicycleList = JsonConvert.DeserializeObject<Bicycles>(apiResponse);
                }
            }
            if (SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "cart") == null)
            {
                List<Item> cart = new List<Item>();
                cart.Add(new Item { Bicycles = bicycleList, Quantity = 1 });
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            else
            {
                List<Item> cart = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "cart");
                int index = isExist(id);
                if (index != -1)
                {
                    cart[index].Quantity++;
                }
                else
                {
                    cart.Add(new Item { Bicycles = bicycleList, Quantity = 1 });
                }
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            return RedirectToAction("Index");
        }

        [Route("remove/{id}")]
        public IActionResult Remove(decimal id)
        {
            try
            {
                if (SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "cart") != null)
                {
                    List<Item> cart = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "cart");
                    int index = isExist(id);
                    if (index != -1)
                    {
                        cart.RemoveAt(index);
                        SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
                    }
                }
            }
            catch
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        private int isExist(decimal id)
        {
            if (SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "cart") != null)
            {
                List<Item> cart = SessionHelper.GetObjectFromJson<List<Item>>(HttpContext.Session, "cart");
                for (int i = 0; i < cart.Count; i++)
                {
                    if (cart[i].Bicycles.Serialnumber.Equals(id))
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
    }
}