﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Bikeparts.cs
 * Summary: Serves as a model for a Bikeparts
 */
using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Bikeparts
    {
        public decimal Serialnumber { get; set; }
        public decimal Componentid { get; set; }
        public decimal? Substituteid { get; set; }
        public string Location { get; set; }
        public decimal? Quantity { get; set; }
        public DateTime? Dateinstalled { get; set; }
        public decimal? Employeeid { get; set; }

        public virtual Component Component { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual Bicycles SerialnumberNavigation { get; set; }
    }
}
