﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Customertransaction.cs
 * Summary: Serves as a model for a Customertransaction
 */
using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Customertransaction
    {
        public decimal Customerid { get; set; }
        public DateTime Transactiondate { get; set; }
        public decimal? Employeeid { get; set; }
        public decimal? Amount { get; set; }
        public string Description { get; set; }
        public decimal? Reference { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
