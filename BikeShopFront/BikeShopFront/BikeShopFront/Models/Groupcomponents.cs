﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Groupcomponents.cs
 * Summary: Serves as a model for a Groupcomponents
 */
using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Groupcomponents
    {
        public decimal Groupid { get; set; }
        public decimal Componentid { get; set; }

        public virtual Component Component { get; set; }
        public virtual Groupo Group { get; set; }
    }
}
