﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Groupo.cs
 * Summary: Serves as a model for a Groupo
 */
using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Groupo
    {
        public Groupo()
        {
            Groupcomponents = new HashSet<Groupcomponents>();
        }

        public decimal Componentgroupid { get; set; }
        public string Groupname { get; set; }
        public string Biketype { get; set; }
        public decimal? Year { get; set; }
        public decimal? Endyear { get; set; }
        public decimal? Weight { get; set; }

        public virtual ICollection<Groupcomponents> Groupcomponents { get; set; }
    }
}
