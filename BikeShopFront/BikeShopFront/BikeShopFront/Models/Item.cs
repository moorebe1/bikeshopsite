﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BikeShopFront.Models
{
    public class Item
    {
        public Bicycles Bicycles { get; set; }
        public int Quantity { get; set; }
    }
}
