﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Letterstyle
 * Summary: Serves as a model for a Letterstyle 
 */
using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Letterstyle
    {
        public Letterstyle()
        {
            Bicycle = new HashSet<Bicycles>();
        }

        public string Letterstyle1 { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Bicycles> Bicycle { get; set; }
    }
}
