﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Modelsize
 * Summary: Serves as a model for a Modelsize 
 */
using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Modelsize
    {
        public string Modeltype { get; set; }
        public decimal Msize { get; set; }
        public decimal? Toptube { get; set; }
        public decimal? Chainstay { get; set; }
        public decimal? Totallength { get; set; }
        public decimal? Groundclearance { get; set; }
        public decimal? Headtubeangle { get; set; }
        public decimal? Seattubeangle { get; set; }

        public virtual Modeltype ModeltypeNavigation { get; set; }
    }
}
