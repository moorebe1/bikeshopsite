﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Purchaseitem
 * Summary: Serves as a model for a Purchaseitem 
 */

using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Purchaseitem
    {
        public decimal Purchaseid { get; set; }
        public decimal Componentid { get; set; }
        public decimal? Pricepaid { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Quantityreceived { get; set; }

        public virtual Component Component { get; set; }
        public virtual Purchaseorder Purchase { get; set; }
    }
}
