﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Purchaseorder
 * Summary: Serves as a model for a Purchaseorder 
 */

using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Purchaseorder
    {
        public Purchaseorder()
        {
            Purchaseitem = new HashSet<Purchaseitem>();
        }

        public decimal Purchaseid { get; set; }
        public decimal? Employeeid { get; set; }
        public decimal? Manufacturerid { get; set; }
        public decimal? Totallist { get; set; }
        public decimal? Shippingcost { get; set; }
        public decimal? Discount { get; set; }
        public DateTime? Orderdate { get; set; }
        public DateTime? Receivedate { get; set; }
        public decimal? Amountdue { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
        public virtual ICollection<Purchaseitem> Purchaseitem { get; set; }
    }
}
