﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Retailstore
 * Summary: Serves as a model for a Retailstore 
 */
using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Retailstore
    {
        public Retailstore()
        {
            Bicycle = new HashSet<Bicycles>();
        }

        public decimal Storeid { get; set; }
        public string Storename { get; set; }
        public string Phone { get; set; }
        public string Contactfirstname { get; set; }
        public string Contactlastname { get; set; }
        public string Address { get; set; }
        public string Zipcode { get; set; }
        public decimal? Cityid { get; set; }

        public virtual City City { get; set; }
        public virtual ICollection<Bicycles> Bicycle { get; set; }
    }
}
