﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Revisionhistory
 * Summary: Serves as a model for a Revisionhistory 
 */
using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Revisionhistory
    {
        public decimal Id { get; set; }
        public string Version { get; set; }
        public DateTime? Changedate { get; set; }
        public string Name { get; set; }
        public string Revisioncomments { get; set; }
    }
}
