﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: samplestreet
 * Summary: Serves as a model for a samplestreet 
 */
using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Samplestreet
    {
        public decimal Id { get; set; }
        public string Baseaddress { get; set; }
    }
}
