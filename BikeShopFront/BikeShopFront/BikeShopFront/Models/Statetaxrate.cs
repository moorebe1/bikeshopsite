﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Statetaxrate
 * Summary: Serves as a model for a Statetaxrate
 */
using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Statetaxrate
    {
        public string State { get; set; }
        public decimal? Taxrate { get; set; }
    }
}
