﻿using BikeShopFront.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BikeShopUnitTests
{

    [TestClass]
    public class CartControllerTests
    {
        [TestMethod]
        public void Index()
        {
            CartController controller = new CartController();

            IActionResult result = controller.Index() as IActionResult;

            NUnit.Framework.Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Buy()
        {

            CartController controller = new CartController();

            Task<IActionResult> result = controller.Buy(12) as Task<IActionResult>;

            NUnit.Framework.Assert.IsNotNull(result);

        }

        [TestMethod]
        public void Remove()
        {
           

            CartController controller = new CartController();

            IActionResult result = controller.Remove(14);

            NUnit.Framework.Assert.IsNotNull(result);

        }
    }
}
