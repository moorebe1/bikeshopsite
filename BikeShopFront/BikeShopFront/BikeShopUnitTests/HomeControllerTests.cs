using BikeShopFront.Controllers;
using BikeShopFront.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System.Threading.Tasks;

namespace BikeShopUnitTests
{
    [TestClass]
    public class HomeControllerTests
    {
       
        [TestMethod]
        public void GetBike()
        {
            
            HomeController controller = new HomeController();

            Task<IActionResult> result = controller.GetBike(12) as Task<IActionResult>;

            NUnit.Framework.Assert.IsNotNull(result);
         
        }


        [TestMethod]
        public void Search()
        {

            HomeController controller = new HomeController();

            Task<IActionResult> result = controller.Search(100, 4000) as Task<IActionResult>;

            NUnit.Framework.Assert.IsNotNull(result);

        }

        [TestMethod]
        public void CreateBike()
        {

            Bicycles bike = new Bicycles();
            bike.Serialnumber = 8675309;
            bike.Saleprice = 7000;
            bike.Listprice = 8000;


            HomeController controller = new HomeController();

            Task<IActionResult> result = controller.CreateBike(bike) as Task<IActionResult>;

            NUnit.Framework.Assert.IsNotNull(result);

        }

        [TestMethod]
        public void UpdateBike()
        {

            Bicycles bike = new Bicycles();
            bike.Serialnumber = 8675309;
            bike.Saleprice = 7000;
            bike.Listprice = 8000;


            HomeController controller = new HomeController();

            Task<IActionResult> result = controller.UpdateBike(8675309) as Task<IActionResult>;

            NUnit.Framework.Assert.IsNotNull(result);

        }

        [TestMethod]
        public void DeleteBike()
        {

            Bicycles bike = new Bicycles();
            bike.Serialnumber = 8675309;
            bike.Saleprice = 7000;
            bike.Listprice = 8000;


            HomeController controller = new HomeController();

            Task<IActionResult> result = controller.DeleteBike(8675309) as Task<IActionResult>;

            NUnit.Framework.Assert.IsNotNull(result);

        }

        [TestMethod]
        public void DeleteconfirmBike()
        {

            Bicycles bike = new Bicycles();
            bike.Serialnumber = 8675309;
            bike.Saleprice = 7000;
            bike.Listprice = 8000;


            HomeController controller = new HomeController();

            Task<IActionResult> result = controller.DeleteBikeConfirmed(8675309) as Task<IActionResult>;

            NUnit.Framework.Assert.IsNotNull(result);

        }
    

    }
}
