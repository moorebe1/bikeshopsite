﻿using BikeShopFront.Areas.Identity.Pages.Account;
using BikeShopFront.Data;
using BikeShopFront.Models;
using Core_RBS.Controllers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BikeShopUnitTests
{
    [TestClass]
    //[TestFixture]
    public class RoleTest
    {
        protected Microsoft.VisualStudio.TestTools.UnitTesting.TestContext db;
        protected Context _db;
        protected SignInManager<IdentityUser> signInManager;
        protected ILogger<LoginModel> logger;
        protected RoleManager<IdentityRole> roleManager;
        protected UserManager<IdentityUser> userManager;
        protected IHostingEnvironment hostingEnvironment;

        //[TestCase]
        [TestMethod]
        public void Create()
        {

            IdentityRole role = new IdentityRole();
            role.Id = "789";
            role.Name = "CEO";
            RoleController controller = new RoleController(roleManager, userManager);
            Task<IActionResult> actionResult = controller.Create(role) as Task<IActionResult>;
            NUnit.Framework.Assert.IsNotNull(actionResult);
        }

        [TestMethod]
        public void Update()
        {

            IdentityRole role = new IdentityRole();
            role.Id = "789";
            role.Name = "CEO";
            RoleController controller = new RoleController(roleManager, userManager);
            Task<IActionResult> actionResult = controller.Update("789") as Task<IActionResult>;
            NUnit.Framework.Assert.IsNotNull(actionResult);
        }

        [TestMethod]
        public void Delete()
        {

            IdentityRole role = new IdentityRole();
            role.Id = "789";
            role.Name = "CEO";
            RoleController controller = new RoleController(roleManager, userManager);
            Task<IActionResult> actionResult = controller.Delete("789") as Task<IActionResult>;
            NUnit.Framework.Assert.IsNotNull(actionResult);
        }

    }
    
}
