﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BikeShopDatabaseAPI.Models
{
    public partial class BikeShopContext : DbContext
    {
        // not needed 
        //public BikeShopContext()
        //{
        //}

        public BikeShopContext(DbContextOptions<BikeShopContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Bicycle> Bicycle { get; set; }
        public virtual DbSet<Bicycletubeusage> Bicycletubeusage { get; set; }
        public virtual DbSet<Bikeparts> Bikeparts { get; set; }
        public virtual DbSet<Biketubes> Biketubes { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Commonsizes> Commonsizes { get; set; }
        public virtual DbSet<Component> Component { get; set; }
        public virtual DbSet<Componentname> Componentname { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Customertransaction> Customertransaction { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Groupcomponents> Groupcomponents { get; set; }
        public virtual DbSet<Groupo> Groupo { get; set; }
        public virtual DbSet<Letterstyle> Letterstyle { get; set; }
        public virtual DbSet<Manufacturer> Manufacturer { get; set; }
        public virtual DbSet<Manufacturertransaction> Manufacturertransaction { get; set; }
        public virtual DbSet<Modelsize> Modelsize { get; set; }
        public virtual DbSet<Modeltype> Modeltype { get; set; }
        public virtual DbSet<Paint> Paint { get; set; }
        public virtual DbSet<Preference> Preference { get; set; }
        public virtual DbSet<Purchaseitem> Purchaseitem { get; set; }
        public virtual DbSet<Purchaseorder> Purchaseorder { get; set; }
        public virtual DbSet<Retailstore> Retailstore { get; set; }
        public virtual DbSet<Revisionhistory> Revisionhistory { get; set; }
        public virtual DbSet<Samplename> Samplename { get; set; }
        public virtual DbSet<Samplestreet> Samplestreet { get; set; }
        public virtual DbSet<Statetaxrate> Statetaxrate { get; set; }
        public virtual DbSet<Tempdatemade> Tempdatemade { get; set; }
        public virtual DbSet<Tubematerial> Tubematerial { get; set; }
        public virtual DbSet<Workarea> Workarea { get; set; }

        // Connection String has been moved to appsettings.json, not neededhere
//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseOracle("USER ID=ADMIN;PASSWORD=TableDropBikes1;DATA SOURCE=(description=(address=(protocol=tcps)(port=1522)(host=adb.us-ashburn-1.oraclecloud.com))(connect_data=(service_name=qc36klymdwahvx7_bikeshop_high.adwc.oraclecloud.com))(SECURITY = (MY_WALLET_DIRECTORY = C:/Users/alexw/Documents/tdb_repository/BikeshopDatabaseAPI/DBWallet)));");
//            }
//        }

        /// <summary>
        /// Removed ALL the usless error code from all "SDWError..." Models
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:DefaultSchema", "ADMIN");

            modelBuilder.Entity<Bicycle>(entity =>
            {
                entity.HasKey(e => e.Serialnumber);

                entity.ToTable("BICYCLE");

                entity.Property(e => e.Serialnumber)
                    .HasColumnName("SERIALNUMBER")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Chainstay)
                    .HasColumnName("CHAINSTAY")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Componentlist)
                    .HasColumnName("COMPONENTLIST")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql(@"0
   ");

                entity.Property(e => e.Construction)
                    .HasColumnName("CONSTRUCTION")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'Bonded'");

                entity.Property(e => e.Customerid)
                    .HasColumnName("CUSTOMERID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Customname)
                    .HasColumnName("CUSTOMNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Employeeid)
                    .HasColumnName("EMPLOYEEID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Frameassembler)
                    .HasColumnName("FRAMEASSEMBLER")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Frameprice)
                    .HasColumnName("FRAMEPRICE")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Framesize)
                    .HasColumnName("FRAMESIZE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Headtubeangle)
                    .HasColumnName("HEADTUBEANGLE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Letterstyleid)
                    .HasColumnName("LETTERSTYLEID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Listprice)
                    .HasColumnName("LISTPRICE")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Modeltype)
                    .HasColumnName("MODELTYPE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Orderdate)
                    .HasColumnName("ORDERDATE")
                    .HasColumnType("DATE");

                entity.Property(e => e.Painter)
                    .HasColumnName("PAINTER")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Paintid)
                    .HasColumnName("PAINTID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Saleprice)
                    .HasColumnName("SALEPRICE")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Salestate)
                    .HasColumnName("SALESTATE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Salestax)
                    .HasColumnName("SALESTAX")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Seattubeangle)
                    .HasColumnName("SEATTUBEANGLE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Shipdate)
                    .HasColumnName("SHIPDATE")
                    .HasColumnType("DATE");

                entity.Property(e => e.Shipemployee)
                    .HasColumnName("SHIPEMPLOYEE")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Shipprice)
                    .HasColumnName("SHIPPRICE")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Startdate)
                    .HasColumnName("STARTDATE")
                    .HasColumnType("DATE");

                entity.Property(e => e.Storeid)
                    .HasColumnName("STOREID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Toptube)
                    .HasColumnName("TOPTUBE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Waterbottlebrazeons)
                    .HasColumnName("WATERBOTTLEBRAZEONS")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("4");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Bicycle)
                    .HasForeignKey(d => d.Customerid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_BIKECUSTOMER");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Bicycle)
                    .HasForeignKey(d => d.Employeeid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_BIKEEMPLOYEE");

                entity.HasOne(d => d.Letterstyle)
                    .WithMany(p => p.Bicycle)
                    .HasForeignKey(d => d.Letterstyleid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_BIKELETTER");

                entity.HasOne(d => d.ModeltypeNavigation)
                    .WithMany(p => p.Bicycle)
                    .HasForeignKey(d => d.Modeltype)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_BIKEMODELTYPE");

                entity.HasOne(d => d.Paint)
                    .WithMany(p => p.Bicycle)
                    .HasForeignKey(d => d.Paintid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_BIKEPAINT");

                entity.HasOne(d => d.Store)
                    .WithMany(p => p.Bicycle)
                    .HasForeignKey(d => d.Storeid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_BIKERETAIL");
            });

            modelBuilder.Entity<Bicycletubeusage>(entity =>
            {
                entity.HasKey(e => new { e.Serialnumber, e.Tubeid });

                entity.ToTable("BICYCLETUBEUSAGE");

                entity.Property(e => e.Serialnumber)
                    .HasColumnName("SERIALNUMBER")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Tubeid)
                    .HasColumnName("TUBEID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Quantity)
                    .HasColumnName("QUANTITY")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql(@"0
   ");

                entity.HasOne(d => d.SerialnumberNavigation)
                    .WithMany(p => p.Bicycletubeusage)
                    .HasForeignKey(d => d.Serialnumber)
                    .HasConstraintName("FK_REFERENCE26");

                entity.HasOne(d => d.Tube)
                    .WithMany(p => p.Bicycletubeusage)
                    .HasForeignKey(d => d.Tubeid)
                    .HasConstraintName("FK_REFERENCE27");
            });

            modelBuilder.Entity<Bikeparts>(entity =>
            {
                entity.HasKey(e => new { e.Serialnumber, e.Componentid });

                entity.ToTable("BIKEPARTS");

                entity.Property(e => e.Serialnumber)
                    .HasColumnName("SERIALNUMBER")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Componentid)
                    .HasColumnName("COMPONENTID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Dateinstalled)
                    .HasColumnName("DATEINSTALLED")
                    .HasColumnType("DATE");

                entity.Property(e => e.Employeeid)
                    .HasColumnName("EMPLOYEEID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql(@"0
   ");

                entity.Property(e => e.Location)
                    .HasColumnName("LOCATION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity)
                    .HasColumnName("QUANTITY")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Substituteid)
                    .HasColumnName("SUBSTITUTEID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.Component)
                    .WithMany(p => p.Bikeparts)
                    .HasForeignKey(d => d.Componentid)
                    .HasConstraintName("FK_REFERENCE5");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Bikeparts)
                    .HasForeignKey(d => d.Employeeid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_REFERENCE4");

                entity.HasOne(d => d.SerialnumberNavigation)
                    .WithMany(p => p.Bikeparts)
                    .HasForeignKey(d => d.Serialnumber)
                    .HasConstraintName("FK_REFERENCE24");
            });

            modelBuilder.Entity<Biketubes>(entity =>
            {
                entity.HasKey(e => new { e.Serialnumber, e.Tubename });

                entity.ToTable("BIKETUBES");

                entity.Property(e => e.Serialnumber)
                    .HasColumnName("SERIALNUMBER")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Tubename)
                    .HasColumnName("TUBENAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Length)
                    .HasColumnName("LENGTH")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql(@"0
   ");

                entity.Property(e => e.Tubeid)
                    .HasColumnName("TUBEID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.SerialnumberNavigation)
                    .WithMany(p => p.Biketubes)
                    .HasForeignKey(d => d.Serialnumber)
                    .HasConstraintName("FK_REFERENCE6");

                entity.HasOne(d => d.Tube)
                    .WithMany(p => p.Biketubes)
                    .HasForeignKey(d => d.Tubeid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_TUBEMATERIALBIKETUBES");
            });

            modelBuilder.Entity<City>(entity =>
            {
                entity.ToTable("CITY");

                entity.Property(e => e.Cityid)
                    .HasColumnName("CITYID")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Areacode)
                    .HasColumnName("AREACODE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City1)
                    .HasColumnName("CITY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .HasColumnName("COUNTRY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Latitude)
                    .HasColumnName("LATITUDE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Longitude)
                    .HasColumnName("LONGITUDE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Population1980)
                    .HasColumnName("POPULATION1980")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Population1990)
                    .HasColumnName("POPULATION1990")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Populationcdf)
                    .HasColumnName("POPULATIONCDF")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql(@"0
   ");

                entity.Property(e => e.State)
                    .HasColumnName("STATE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCODE")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Commonsizes>(entity =>
            {
                entity.HasKey(e => new { e.Modeltype, e.Framesize });

                entity.ToTable("COMMONSIZES");

                entity.Property(e => e.Modeltype)
                    .HasColumnName("MODELTYPE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Framesize)
                    .HasColumnName("FRAMESIZE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql(@"0
   ");
            });

            modelBuilder.Entity<Component>(entity =>
            {
                entity.ToTable("COMPONENT");

                entity.Property(e => e.Componentid)
                    .HasColumnName("COMPONENTID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Category)
                    .HasColumnName("CATEGORY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Endyear)
                    .HasColumnName("ENDYEAR")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Estimatedcost)
                    .HasColumnName("ESTIMATEDCOST")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Height)
                    .HasColumnName("HEIGHT")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Length)
                    .HasColumnName("LENGTH")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Listprice)
                    .HasColumnName("LISTPRICE")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Manufacturerid)
                    .HasColumnName("MANUFACTURERID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Productnumber)
                    .HasColumnName("PRODUCTNUMBER")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Quantityonhand)
                    .HasColumnName("QUANTITYONHAND")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql(@"10
   ");

                entity.Property(e => e.Road)
                    .HasColumnName("ROAD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Weight)
                    .HasColumnName("WEIGHT")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Width)
                    .HasColumnName("WIDTH")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Year)
                    .HasColumnName("YEAR")
                    .HasColumnType("NUMBER(38)");

                entity.HasOne(d => d.CategoryNavigation)
                    .WithMany(p => p.Component)
                    .HasForeignKey(d => d.Category)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_REFERENCE");

                entity.HasOne(d => d.Manufacturer)
                    .WithMany(p => p.Component)
                    .HasForeignKey(d => d.Manufacturerid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_REFERENCE16");
            });

            modelBuilder.Entity<Componentname>(entity =>
            {
                entity.HasKey(e => e.Componentname1);

                entity.ToTable("COMPONENTNAME");

                entity.Property(e => e.Componentname1)
                    .HasColumnName("COMPONENTNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Assemblyorder)
                    .HasColumnName("ASSEMBLYORDER")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("CUSTOMER");

                entity.Property(e => e.Customerid)
                    .HasColumnName("CUSTOMERID")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Address)
                    .HasColumnName("ADDRESS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Balancedue)
                    .HasColumnName("BALANCEDUE")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql(@"0
   ");

                entity.Property(e => e.Cityid)
                    .HasColumnName("CITYID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Firstname)
                    .HasColumnName("FIRSTNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Lastname)
                    .HasColumnName("LASTNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("PHONE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCODE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.Cityid)
                    .HasConstraintName("FK_CITYCUSTOMER");
            });

            modelBuilder.Entity<Customertransaction>(entity =>
            {
                entity.HasKey(e => new { e.Customerid, e.Transactiondate });

                entity.ToTable("CUSTOMERTRANSACTION");

                entity.Property(e => e.Customerid)
                    .HasColumnName("CUSTOMERID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Transactiondate)
                    .HasColumnName("TRANSACTIONDATE")
                    .HasColumnType("DATE");

                entity.Property(e => e.Amount)
                    .HasColumnName("AMOUNT")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Employeeid)
                    .HasColumnName("EMPLOYEEID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Reference)
                    .HasColumnName("REFERENCE")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql(@"0
   ");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Customertransaction)
                    .HasForeignKey(d => d.Customerid)
                    .HasConstraintName("FK_REFERENCE18");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("EMPLOYEE");

                entity.Property(e => e.Employeeid)
                    .HasColumnName("EMPLOYEEID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Address)
                    .HasColumnName("ADDRESS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Cityid)
                    .HasColumnName("CITYID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Currentmanager)
                    .HasColumnName("CURRENTMANAGER")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Datehired)
                    .HasColumnName("DATEHIRED")
                    .HasColumnType("DATE");

                entity.Property(e => e.Datereleased)
                    .HasColumnName("DATERELEASED")
                    .HasColumnType("DATE");

                entity.Property(e => e.Firstname)
                    .HasColumnName("FIRSTNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Homephone)
                    .HasColumnName("HOMEPHONE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Lastname)
                    .HasColumnName("LASTNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Salary)
                    .HasColumnName("SALARY")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Salarygrade)
                    .HasColumnName("SALARYGRADE")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Taxpayerid)
                    .HasColumnName("TAXPAYERID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasColumnName("TITLE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Workarea)
                    .HasColumnName("WORKAREA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCODE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Employee)
                    .HasForeignKey(d => d.Cityid)
                    .HasConstraintName("FK_CITYEMPLOYEE");
            });

            modelBuilder.Entity<Groupcomponents>(entity =>
            {
                entity.HasKey(e => new { e.Groupid, e.Componentid });

                entity.ToTable("GROUPCOMPONENTS");

                entity.Property(e => e.Groupid)
                    .HasColumnName("GROUPID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Componentid)
                    .HasColumnName("COMPONENTID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql(@"0
   ");

                entity.HasOne(d => d.Component)
                    .WithMany(p => p.Groupcomponents)
                    .HasForeignKey(d => d.Componentid)
                    .HasConstraintName("FK_REFERENCE14");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Groupcomponents)
                    .HasForeignKey(d => d.Groupid)
                    .HasConstraintName("FK_REFERENCE15");
            });

            modelBuilder.Entity<Groupo>(entity =>
            {
                entity.HasKey(e => e.Componentgroupid);

                entity.ToTable("GROUPO");

                entity.Property(e => e.Componentgroupid)
                    .HasColumnName("COMPONENTGROUPID")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Biketype)
                    .HasColumnName("BIKETYPE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Endyear)
                    .HasColumnName("ENDYEAR")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Groupname)
                    .HasColumnName("GROUPNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Weight)
                    .HasColumnName("WEIGHT")
                    .HasColumnType("NUMBER(15,4)");

                entity.Property(e => e.Year)
                    .HasColumnName("YEAR")
                    .HasColumnType("NUMBER(38)");
            });

            modelBuilder.Entity<Letterstyle>(entity =>
            {
                entity.HasKey(e => e.Letterstyle1);

                entity.ToTable("LETTERSTYLE");

                entity.Property(e => e.Letterstyle1)
                    .HasColumnName("LETTERSTYLE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Manufacturer>(entity =>
            {
                entity.ToTable("MANUFACTURER");

                entity.Property(e => e.Manufacturerid)
                    .HasColumnName("MANUFACTURERID")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Address)
                    .HasColumnName("ADDRESS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Balancedue)
                    .HasColumnName("BALANCEDUE")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql(@"0
   ");

                entity.Property(e => e.Cityid)
                    .HasColumnName("CITYID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Contactname)
                    .HasColumnName("CONTACTNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Manufacturername)
                    .HasColumnName("MANUFACTURERNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("PHONE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCODE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Manufacturer)
                    .HasForeignKey(d => d.Cityid)
                    .HasConstraintName("FK_CITYMANUFACTURER");
            });

            modelBuilder.Entity<Manufacturertransaction>(entity =>
            {
                entity.HasKey(e => new { e.Manufacturerid, e.Transactiondate, e.Reference })
                    .HasName("PK_MANUFTRANSACTION");

                entity.ToTable("MANUFACTURERTRANSACTION");

                entity.Property(e => e.Manufacturerid)
                    .HasColumnName("MANUFACTURERID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Transactiondate)
                    .HasColumnName("TRANSACTIONDATE")
                    .HasColumnType("DATE");

                entity.Property(e => e.Reference)
                    .HasColumnName("REFERENCE")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql(@"0
   ");

                entity.Property(e => e.Amount)
                    .HasColumnName("AMOUNT")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Employeeid)
                    .HasColumnName("EMPLOYEEID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.Manufacturer)
                    .WithMany(p => p.Manufacturertransaction)
                    .HasForeignKey(d => d.Manufacturerid)
                    .HasConstraintName("FK_MANUFTRANS");
            });

            modelBuilder.Entity<Modelsize>(entity =>
            {
                entity.HasKey(e => new { e.Modeltype, e.Msize });

                entity.ToTable("MODELSIZE");

                entity.Property(e => e.Modeltype)
                    .HasColumnName("MODELTYPE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Msize)
                    .HasColumnName("MSIZE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Chainstay)
                    .HasColumnName("CHAINSTAY")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Groundclearance)
                    .HasColumnName("GROUNDCLEARANCE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Headtubeangle)
                    .HasColumnName("HEADTUBEANGLE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Seattubeangle)
                    .HasColumnName("SEATTUBEANGLE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql(@"0
   ");

                entity.Property(e => e.Toptube)
                    .HasColumnName("TOPTUBE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Totallength)
                    .HasColumnName("TOTALLENGTH")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.ModeltypeNavigation)
                    .WithMany(p => p.Modelsize)
                    .HasForeignKey(d => d.Modeltype)
                    .HasConstraintName("FK_MODELTYPE");
            });

            modelBuilder.Entity<Modeltype>(entity =>
            {
                entity.HasKey(e => e.Modeltype1);

                entity.ToTable("MODELTYPE");

                entity.Property(e => e.Modeltype1)
                    .HasColumnName("MODELTYPE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Componentid)
                    .HasColumnName("COMPONENTID")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Paint>(entity =>
            {
                entity.ToTable("PAINT");

                entity.Property(e => e.Paintid)
                    .HasColumnName("PAINTID")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Colorlist)
                    .HasColumnName("COLORLIST")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Colorname)
                    .HasColumnName("COLORNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Colorstyle)
                    .HasColumnName("COLORSTYLE")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'Solid'");

                entity.Property(e => e.Datediscontinued)
                    .HasColumnName("DATEDISCONTINUED")
                    .HasColumnType("DATE");

                entity.Property(e => e.Dateintroduced)
                    .HasColumnName("DATEINTRODUCED")
                    .HasColumnType("DATE");
            });

            modelBuilder.Entity<Preference>(entity =>
            {
                entity.HasKey(e => e.Itemname);

                entity.ToTable("PREFERENCE");

                entity.Property(e => e.Itemname)
                    .HasColumnName("ITEMNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Datechanged)
                    .HasColumnName("DATECHANGED")
                    .HasColumnType("DATE");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasColumnName("VALUE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Purchaseitem>(entity =>
            {
                entity.HasKey(e => new { e.Purchaseid, e.Componentid });

                entity.ToTable("PURCHASEITEM");

                entity.Property(e => e.Purchaseid)
                    .HasColumnName("PURCHASEID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Componentid)
                    .HasColumnName("COMPONENTID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Pricepaid)
                    .HasColumnName("PRICEPAID")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Quantity)
                    .HasColumnName("QUANTITY")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Quantityreceived)
                    .HasColumnName("QUANTITYRECEIVED")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql(@"0
   ");

                entity.HasOne(d => d.Component)
                    .WithMany(p => p.Purchaseitem)
                    .HasForeignKey(d => d.Componentid)
                    .HasConstraintName("FK_REFERENCE21");

                entity.HasOne(d => d.Purchase)
                    .WithMany(p => p.Purchaseitem)
                    .HasForeignKey(d => d.Purchaseid)
                    .HasConstraintName("FK_REFERENCE20");
            });

            modelBuilder.Entity<Purchaseorder>(entity =>
            {
                entity.HasKey(e => e.Purchaseid);

                entity.ToTable("PURCHASEORDER");

                entity.Property(e => e.Purchaseid)
                    .HasColumnName("PURCHASEID")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Amountdue)
                    .HasColumnName("AMOUNTDUE")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql(@"0
   ");

                entity.Property(e => e.Discount)
                    .HasColumnName("DISCOUNT")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Employeeid)
                    .HasColumnName("EMPLOYEEID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Manufacturerid)
                    .HasColumnName("MANUFACTURERID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Orderdate)
                    .HasColumnName("ORDERDATE")
                    .HasColumnType("DATE");

                entity.Property(e => e.Receivedate)
                    .HasColumnName("RECEIVEDATE")
                    .HasColumnType("DATE");

                entity.Property(e => e.Shippingcost)
                    .HasColumnName("SHIPPINGCOST")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Totallist)
                    .HasColumnName("TOTALLIST")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.Purchaseorder)
                    .HasForeignKey(d => d.Employeeid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_REFERENCE23");

                entity.HasOne(d => d.Manufacturer)
                    .WithMany(p => p.Purchaseorder)
                    .HasForeignKey(d => d.Manufacturerid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_REFERENCE22");
            });

            modelBuilder.Entity<Retailstore>(entity =>
            {
                entity.HasKey(e => e.Storeid);

                entity.ToTable("RETAILSTORE");

                entity.Property(e => e.Storeid)
                    .HasColumnName("STOREID")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Address)
                    .HasColumnName("ADDRESS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Cityid)
                    .HasColumnName("CITYID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql(@"0
   ");

                entity.Property(e => e.Contactfirstname)
                    .HasColumnName("CONTACTFIRSTNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Contactlastname)
                    .HasColumnName("CONTACTLASTNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasColumnName("PHONE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Storename)
                    .HasColumnName("STORENAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("ZIPCODE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Retailstore)
                    .HasForeignKey(d => d.Cityid)
                    .HasConstraintName("FK_CITYRETAILSTORE");
            });

            modelBuilder.Entity<Revisionhistory>(entity =>
            {
                entity.ToTable("REVISIONHISTORY");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Changedate)
                    .HasColumnName("CHANGEDATE")
                    .HasColumnType("DATE");

                entity.Property(e => e.Name)
                    .HasColumnName("NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Revisioncomments)
                    .HasColumnName("REVISIONCOMMENTS")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasColumnName("VERSION")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Samplename>(entity =>
            {
                entity.ToTable("SAMPLENAME");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Firstname)
                    .HasColumnName("FIRSTNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasColumnName("GENDER")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Lastname)
                    .HasColumnName("LASTNAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Samplestreet>(entity =>
            {
                entity.ToTable("SAMPLESTREET");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER(38)");

                entity.Property(e => e.Baseaddress)
                    .HasColumnName("BASEADDRESS")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Statetaxrate>(entity =>
            {
                entity.HasKey(e => e.State);

                entity.ToTable("STATETAXRATE");

                entity.Property(e => e.State)
                    .HasColumnName("STATE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Taxrate)
                    .HasColumnName("TAXRATE")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql(@"0
   ");
            });

            modelBuilder.Entity<Tempdatemade>(entity =>
            {
                entity.HasKey(e => e.Datevalue);

                entity.ToTable("TEMPDATEMADE");

                entity.Property(e => e.Datevalue)
                    .HasColumnName("DATEVALUE")
                    .HasColumnType("DATE");

                entity.Property(e => e.Madecount)
                    .HasColumnName("MADECOUNT")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql(@"0
   ");
            });

            modelBuilder.Entity<Tubematerial>(entity =>
            {
                entity.HasKey(e => e.Tubeid);

                entity.ToTable("TUBEMATERIAL");

                entity.Property(e => e.Tubeid)
                    .HasColumnName("TUBEID")
                    .HasColumnType("NUMBER(38)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Construction)
                    .HasColumnName("CONSTRUCTION")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql(@"'Bonded'
   ");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Diameter)
                    .HasColumnName("DIAMETER")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Listprice)
                    .HasColumnName("LISTPRICE")
                    .HasColumnType("NUMBER(38,4)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Material)
                    .HasColumnName("MATERIAL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Roundness)
                    .HasColumnName("ROUNDNESS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Stiffness)
                    .HasColumnName("STIFFNESS")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Thickness)
                    .HasColumnName("THICKNESS")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Weight)
                    .HasColumnName("WEIGHT")
                    .HasColumnType("NUMBER")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Workarea>(entity =>
            {
                entity.HasKey(e => e.Workarea1);

                entity.ToTable("WORKAREA");

                entity.Property(e => e.Workarea1)
                    .HasColumnName("WORKAREA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
