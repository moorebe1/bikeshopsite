﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: BikepartsController
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikeShopDatabaseAPI.Models;

namespace BikeShopDatabaseAPI.Controllers
{
    ///-------------------------------------------------------------------------------------------------
    /// <summary>   A controller for handling bikeparts. </summary>
    ///
    /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
    ///-------------------------------------------------------------------------------------------------

    [Route("api/[controller]")]
    [ApiController]
    public class BikepartsController : ControllerBase
    {
        /// <summary>   The context. </summary>
        private readonly BikeShopContext _context;

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="context">  The context. </param>
        ///-------------------------------------------------------------------------------------------------

        public BikepartsController(BikeShopContext context)
        {
            _context = context;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Bikeparts. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <returns>   The bikeparts. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Bikeparts>>> GetBikeparts()
        {
            return await _context.Bikeparts.ToListAsync();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Bikeparts/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   The bikeparts. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet("{id}")]
        public async Task<ActionResult<Bikeparts>> GetBikeparts(decimal id)
        {
            var bikeparts = await _context.Bikeparts.FindAsync(id);

            if (bikeparts == null)
            {
                return NotFound();
            }

            return bikeparts;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     PUT: api/Bikeparts/5 To protect from overposting attacks, enable the specific properties
        ///     you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateConcurrencyException"> Thrown when a Database Update Concurrency
        ///                                                 error condition occurs. </exception>
        ///
        /// <param name="id">           The identifier. </param>
        /// <param name="bikeparts">    The bikeparts. </param>
        ///
        /// <returns>   An IActionResult. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPut("{id}")]
        public async Task<IActionResult> PutBikeparts(decimal id, Bikeparts bikeparts)
        {
            if (id != bikeparts.Serialnumber)
            {
                return BadRequest();
            }

            _context.Entry(bikeparts).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BikepartsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     POST: api/Bikeparts To protect from overposting attacks, enable the specific properties
        ///     you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateException">    Thrown when a Database Update error condition occurs. </exception>
        ///
        /// <param name="bikeparts">    The bikeparts. </param>
        ///
        /// <returns>   An ActionResult&lt;Bikeparts&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPost]
        public async Task<ActionResult<Bikeparts>> PostBikeparts(Bikeparts bikeparts)
        {
            _context.Bikeparts.Add(bikeparts);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (BikepartsExists(bikeparts.Serialnumber))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetBikeparts", new { id = bikeparts.Serialnumber }, bikeparts);
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   DELETE: api/Bikeparts/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   An ActionResult&lt;Bikeparts&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpDelete("{id}")]
        public async Task<ActionResult<Bikeparts>> DeleteBikeparts(decimal id)
        {
            var bikeparts = await _context.Bikeparts.FindAsync(id);
            if (bikeparts == null)
            {
                return NotFound();
            }

            _context.Bikeparts.Remove(bikeparts);
            await _context.SaveChangesAsync();

            return bikeparts;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Queries if a given bikeparts exists. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   True if it succeeds, false if it fails. </returns>
        ///-------------------------------------------------------------------------------------------------

        private bool BikepartsExists(decimal id)
        {
            return _context.Bikeparts.Any(e => e.Serialnumber == id);
        }
    }
}
