﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: ManufacturertransactionsController
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikeShopDatabaseAPI.Models;

namespace BikeShopDatabaseAPI.Controllers
{
    ///-------------------------------------------------------------------------------------------------
    /// <summary>   A controller for handling manufacturertransactions. </summary>
    ///
    /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
    ///-------------------------------------------------------------------------------------------------

    [Route("api/[controller]")]
    [ApiController]
    public class ManufacturertransactionsController : ControllerBase
    {
        /// <summary>   The context. </summary>
        private readonly BikeShopContext _context;

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="context">  The context. </param>
        ///-------------------------------------------------------------------------------------------------

        public ManufacturertransactionsController(BikeShopContext context)
        {
            _context = context;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Manufacturertransactions. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <returns>   The manufacturertransaction. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Manufacturertransaction>>> GetManufacturertransaction()
        {
            return await _context.Manufacturertransaction.ToListAsync();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Manufacturertransactions/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   The manufacturertransaction. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet("{id}")]
        public async Task<ActionResult<Manufacturertransaction>> GetManufacturertransaction(decimal id)
        {
            var manufacturertransaction = await _context.Manufacturertransaction.FindAsync(id);

            if (manufacturertransaction == null)
            {
                return NotFound();
            }

            return manufacturertransaction;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     PUT: api/Manufacturertransactions/5 To protect from overposting attacks, enable the
        ///     specific properties you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateConcurrencyException"> Thrown when a Database Update Concurrency
        ///                                                 error condition occurs. </exception>
        ///
        /// <param name="id">                       The identifier. </param>
        /// <param name="manufacturertransaction">  The manufacturertransaction. </param>
        ///
        /// <returns>   An IActionResult. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPut("{id}")]
        public async Task<IActionResult> PutManufacturertransaction(decimal id, Manufacturertransaction manufacturertransaction)
        {
            if (id != manufacturertransaction.Manufacturerid)
            {
                return BadRequest();
            }

            _context.Entry(manufacturertransaction).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ManufacturertransactionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     POST: api/Manufacturertransactions To protect from overposting attacks, enable the
        ///     specific properties you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="manufacturertransaction">  The manufacturertransaction. </param>
        ///
        /// <returns>   An ActionResult&lt;Manufacturertransaction&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPost]
        public async Task<ActionResult<Manufacturertransaction>> PostManufacturertransaction(Manufacturertransaction manufacturertransaction)
        {
            _context.Manufacturertransaction.Add(manufacturertransaction);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetManufacturertransaction", new { id = manufacturertransaction.Manufacturerid }, manufacturertransaction);
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   DELETE: api/Manufacturertransactions/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   An ActionResult&lt;Manufacturertransaction&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpDelete("{id}")]
        public async Task<ActionResult<Manufacturertransaction>> DeleteManufacturertransaction(decimal id)
        {
            var manufacturertransaction = await _context.Manufacturertransaction.FindAsync(id);
            if (manufacturertransaction == null)
            {
                return NotFound();
            }

            _context.Manufacturertransaction.Remove(manufacturertransaction);
            await _context.SaveChangesAsync();

            return manufacturertransaction;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Queries if a given manufacturertransaction exists. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   True if it succeeds, false if it fails. </returns>
        ///-------------------------------------------------------------------------------------------------

        private bool ManufacturertransactionExists(decimal id)
        {
            return _context.Manufacturertransaction.Any(e => e.Manufacturerid == id);
        }
    }
}
