﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: TempdatemadesController
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikeShopDatabaseAPI.Models;

namespace BikeShopDatabaseAPI.Controllers
{
    ///-------------------------------------------------------------------------------------------------
    /// <summary>   A controller for handling tempdatemades. </summary>
    ///
    /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
    ///-------------------------------------------------------------------------------------------------

    [Route("api/[controller]")]
    [ApiController]
    public class TempdatemadesController : ControllerBase
    {
        /// <summary>   The context. </summary>
        private readonly BikeShopContext _context;

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="context">  The context. </param>
        ///-------------------------------------------------------------------------------------------------

        public TempdatemadesController(BikeShopContext context)
        {
            _context = context;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Tempdatemades. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <returns>   The tempdatemade. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Tempdatemade>>> GetTempdatemade()
        {
            return await _context.Tempdatemade.ToListAsync();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Tempdatemades/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier Date/Time. </param>
        ///
        /// <returns>   The tempdatemade. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet("{id}")]
        public async Task<ActionResult<Tempdatemade>> GetTempdatemade(DateTime id)
        {
            var tempdatemade = await _context.Tempdatemade.FindAsync(id);

            if (tempdatemade == null)
            {
                return NotFound();
            }

            return tempdatemade;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     PUT: api/Tempdatemades/5 To protect from overposting attacks, enable the specific
        ///     properties you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateConcurrencyException"> Thrown when a Database Update Concurrency
        ///                                                 error condition occurs. </exception>
        ///
        /// <param name="id">           The identifier Date/Time. </param>
        /// <param name="tempdatemade"> The tempdatemade. </param>
        ///
        /// <returns>   An IActionResult. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPut("{id}")]
        public async Task<IActionResult> PutTempdatemade(DateTime id, Tempdatemade tempdatemade)
        {
            if (id != tempdatemade.Datevalue)
            {
                return BadRequest();
            }

            _context.Entry(tempdatemade).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TempdatemadeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     POST: api/Tempdatemades To protect from overposting attacks, enable the specific
        ///     properties you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateException">    Thrown when a Database Update error condition occurs. </exception>
        ///
        /// <param name="tempdatemade"> The tempdatemade. </param>
        ///
        /// <returns>   An ActionResult&lt;Tempdatemade&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPost]
        public async Task<ActionResult<Tempdatemade>> PostTempdatemade(Tempdatemade tempdatemade)
        {
            _context.Tempdatemade.Add(tempdatemade);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TempdatemadeExists(tempdatemade.Datevalue))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTempdatemade", new { id = tempdatemade.Datevalue }, tempdatemade);
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   DELETE: api/Tempdatemades/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier Date/Time. </param>
        ///
        /// <returns>   An ActionResult&lt;Tempdatemade&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpDelete("{id}")]
        public async Task<ActionResult<Tempdatemade>> DeleteTempdatemade(DateTime id)
        {
            var tempdatemade = await _context.Tempdatemade.FindAsync(id);
            if (tempdatemade == null)
            {
                return NotFound();
            }

            _context.Tempdatemade.Remove(tempdatemade);
            await _context.SaveChangesAsync();

            return tempdatemade;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Queries if a given tempdatemade exists. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier Date/Time. </param>
        ///
        /// <returns>   True if it succeeds, false if it fails. </returns>
        ///-------------------------------------------------------------------------------------------------

        private bool TempdatemadeExists(DateTime id)
        {
            return _context.Tempdatemade.Any(e => e.Datevalue == id);
        }
    }
}
