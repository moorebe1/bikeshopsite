﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Bicycletubeusage.cs
 * Summary: Serves as a model for a Bicycletubeusage
 */
using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class Bicycletubeusage
    {
        public decimal Serialnumber { get; set; }
        public decimal Tubeid { get; set; }
        public decimal? Quantity { get; set; }

        public virtual Bicycle SerialnumberNavigation { get; set; }
        public virtual Tubematerial Tube { get; set; }
    }
}
