﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Biketubes.cs
 * Summary: Serves as a model for a Biketubes
 */
using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class Biketubes
    {
        public decimal Serialnumber { get; set; }
        public string Tubename { get; set; }
        public decimal? Tubeid { get; set; }
        public decimal? Length { get; set; }

        public virtual Bicycle SerialnumberNavigation { get; set; }
        public virtual Tubematerial Tube { get; set; }
    }
}
