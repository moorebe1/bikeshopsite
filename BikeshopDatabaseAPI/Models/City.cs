﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: City.cs
 * Summary: Serves as a model for a City
 */
using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class City
    {
        public City()
        {
            Customer = new HashSet<Customer>();
            Employee = new HashSet<Employee>();
            Manufacturer = new HashSet<Manufacturer>();
            Retailstore = new HashSet<Retailstore>();
        }

        public decimal Cityid { get; set; }
        public string Zipcode { get; set; }
        public string City1 { get; set; }
        public string State { get; set; }
        public string Areacode { get; set; }
        public decimal? Population1990 { get; set; }
        public decimal? Population1980 { get; set; }
        public string Country { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public decimal? Populationcdf { get; set; }

        public virtual ICollection<Customer> Customer { get; set; }
        public virtual ICollection<Employee> Employee { get; set; }
        public virtual ICollection<Manufacturer> Manufacturer { get; set; }
        public virtual ICollection<Retailstore> Retailstore { get; set; }
    }
}
