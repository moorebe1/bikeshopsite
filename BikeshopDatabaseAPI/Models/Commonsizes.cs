﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Commonsizes.cs
 * Summary: Serves as a model for a Commonsize
 */
using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class Commonsizes
    {
        public string Modeltype { get; set; }
        public decimal Framesize { get; set; }
    }
}
