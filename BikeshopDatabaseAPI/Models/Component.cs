﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Component.cs
 * Summary: Serves as a model for a Component
 */
using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class Component
    {
        public Component()
        {
            Bikeparts = new HashSet<Bikeparts>();
            Groupcomponents = new HashSet<Groupcomponents>();
            Purchaseitem = new HashSet<Purchaseitem>();
        }

        public decimal Componentid { get; set; }
        public decimal? Manufacturerid { get; set; }
        public string Productnumber { get; set; }
        public string Road { get; set; }
        public string Category { get; set; }
        public decimal? Length { get; set; }
        public decimal? Height { get; set; }
        public decimal? Width { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Year { get; set; }
        public decimal? Endyear { get; set; }
        public string Description { get; set; }
        public decimal? Listprice { get; set; }
        public decimal? Estimatedcost { get; set; }
        public decimal? Quantityonhand { get; set; }

        public virtual Componentname CategoryNavigation { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
        public virtual ICollection<Bikeparts> Bikeparts { get; set; }
        public virtual ICollection<Groupcomponents> Groupcomponents { get; set; }
        public virtual ICollection<Purchaseitem> Purchaseitem { get; set; }
    }
}
