﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Manufacturer
 * Summary: Serves as a model for a Manufacturer 
 */

using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class Manufacturer
    {
        public Manufacturer()
        {
            Component = new HashSet<Component>();
            Manufacturertransaction = new HashSet<Manufacturertransaction>();
            Purchaseorder = new HashSet<Purchaseorder>();
        }

        public decimal Manufacturerid { get; set; }
        public string Manufacturername { get; set; }
        public string Contactname { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Zipcode { get; set; }
        public decimal? Cityid { get; set; }
        public decimal? Balancedue { get; set; }

        public virtual City City { get; set; }
        public virtual ICollection<Component> Component { get; set; }
        public virtual ICollection<Manufacturertransaction> Manufacturertransaction { get; set; }
        public virtual ICollection<Purchaseorder> Purchaseorder { get; set; }
    }
}
