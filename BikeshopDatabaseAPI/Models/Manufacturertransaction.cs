﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Manufacturertransaction
 * Summary: Serves as a model for a Manufacturertransaction 
 */
using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class Manufacturertransaction
    {
        public decimal Manufacturerid { get; set; }
        public DateTime Transactiondate { get; set; }
        public decimal? Employeeid { get; set; }
        public decimal? Amount { get; set; }
        public string Description { get; set; }
        public decimal Reference { get; set; }

        public virtual Manufacturer Manufacturer { get; set; }
    }
}
