﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: TempDateMade
 * Summary: Serves as a model for a TempDateMade
 */
using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class Tempdatemade
    {
        public DateTime Datevalue { get; set; }
        public decimal? Madecount { get; set; }
    }
}
