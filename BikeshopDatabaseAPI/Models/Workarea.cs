﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: WorkareasController
 * Summary: Serves as a model for a Workareas
 */

using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class Workarea
    {
        public string Workarea1 { get; set; }
        public string Description { get; set; }
    }
}
